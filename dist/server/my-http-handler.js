"use strict";
const Boom = require("boom");
const html_parser_1 = require("./html-parser");
const http_client_1 = require("./http-client");
function onSuccess(url, html, reply) {
    "use strict";
    const parser = new html_parser_1.default(url, html);
    const links = parser.parseLinks();
    const result = {
        hasLoginForm: parser.hasLoginForm(),
        htmlVersion: parser.getHtmlVersion(),
        pageTitle: parser.getPageTitle(),
        headings: parser.getHeadingsCount(),
        internalLinksCount: links.internalLinksCount,
        externalLinksCount: links.externalLinksCount,
        inaccessibleLinksCount: links.inaccessibleLinksCount,
    };
    reply(result);
}
function onError(error, reply) {
    "use strict";
    const httpError = Boom.badData("URL could not be processed");
    reply(httpError);
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (request, reply) => {
    const url = request.payload.url;
    const client = new http_client_1.default();
    client.fetch(url, (html) => onSuccess(url, html, reply), (error) => onError(error, reply));
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXktaHR0cC1oYW5kbGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc2VydmVyL3NyYy9teS1odHRwLWhhbmRsZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE1BQVksSUFBSSxXQUFNLE1BQU0sQ0FBQyxDQUFBO0FBRzdCLDhCQUFtQixlQUFlLENBQUMsQ0FBQTtBQUNuQyw4QkFBdUIsZUFBZSxDQUFDLENBQUE7QUFFdkMsbUJBQW1CLEdBQVcsRUFBRSxJQUFZLEVBQUUsS0FBMEI7SUFDcEUsWUFBWSxDQUFDO0lBRWIsTUFBTSxNQUFNLEdBQUcsSUFBSSxxQkFBTSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNyQyxNQUFNLEtBQUssR0FBRyxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDbEMsTUFBTSxNQUFNLEdBQVk7UUFDcEIsWUFBWSxFQUFFLE1BQU0sQ0FBQyxZQUFZLEVBQUU7UUFDbkMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxjQUFjLEVBQUU7UUFDcEMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxZQUFZLEVBQUU7UUFDaEMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRTtRQUNuQyxrQkFBa0IsRUFBRSxLQUFLLENBQUMsa0JBQWtCO1FBQzVDLGtCQUFrQixFQUFFLEtBQUssQ0FBQyxrQkFBa0I7UUFDNUMsc0JBQXNCLEVBQUUsS0FBSyxDQUFDLHNCQUFzQjtLQUN2RCxDQUFDO0lBQ0YsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ2xCLENBQUM7QUFFRCxpQkFBaUIsS0FBVSxFQUFFLEtBQTBCO0lBQ25ELFlBQVksQ0FBQztJQUNiLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsNEJBQTRCLENBQUMsQ0FBQztJQUM3RCxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDckIsQ0FBQztBQUVEO2tCQUFlLENBQUMsT0FBWSxFQUFFLEtBQVU7SUFDcEMsTUFBTSxHQUFHLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7SUFDaEMsTUFBTSxNQUFNLEdBQUcsSUFBSSxxQkFBVSxFQUFFLENBQUM7SUFDaEMsTUFBTSxDQUFDLEtBQUssQ0FDUixHQUFHLEVBQ0gsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQ3JDLENBQUMsS0FBSyxLQUFLLE9BQU8sQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQ25DLENBQUM7QUFDTixDQUFDLENBQUMifQ==