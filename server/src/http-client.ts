import * as request from "request";

export default class HttpClient {
    public fetch(url: string, onSuccess: (html: string) => void, onError: (error: any) => void): void {
        request(url, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                onSuccess(body);
            } else {
                onError (error);
            }
        });
    }
}
