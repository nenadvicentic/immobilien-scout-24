import * as cheerio from "cheerio";
import * as url from "url";

export default class HtmlParser {
    private $: cheerio.Static;
    private pageUrl: url.Url;

    constructor(pageUrl: string, public html: string) {
        this.$ = cheerio.load(html);
        this.pageUrl = url.parse(pageUrl);
    }

    /** Gets known HTML document type by trying to parse <!doctype> */
    public getHtmlVersion(): string {
        const regex = /<!doctype html( PUBLIC \"-\/\/W3C\/\/DTD (.+?)\/\/\w\w\".*?)?>/gmi;

        const matches = regex.exec(this.html);
        if (matches == null || matches.length === 0) {
            return "Not specified";
        }
        // if no DTD description, default to HTML 5 <!doctype html>
        return matches[2] || "HTML 5";
    }

    /** Reads page title */
    public getPageTitle(): string {
        return this.$("head>title").text();
    }

    public getHeadingsCount(): { name: string, count: number }[] {
        const headings = new Array<{ name: string, count: number }>();

        ["h1", "h2", "h3", "h4", "h5", "h6"].forEach((val) => {
            headings.push({ name: val, count: this.$(val).length });
        });

        return headings;
    }

    /** Counts all internal, external on inacessible links on the page */
    public parseLinks(): { internalLinksCount: number, externalLinksCount: number, inaccessibleLinksCount: number } {
        const result = {
            externalLinksCount: 0,
            inaccessibleLinksCount: 0,
            internalLinksCount: 0,
        };
        this.$("a").each((index, element) => {
            const href = this.$(element).attr("href");
            try {
                const linkUrl = url.parse(href);
                if (linkUrl.hostname == null ||
                    linkUrl.hostname === this.pageUrl.hostname) {
                    result.internalLinksCount++;
                } else {
                    result.externalLinksCount++;
                }
            } catch (ex) {
                // no or mallformed href, inaccessible link
                result.inaccessibleLinksCount++;
            }
        });
        return result;
    }

    /**
     * Searches for login form, based on assumption that login form has exactly one password field.
     * Registration pages usually have 2 password fields.
     */
    public hasLoginForm(): boolean {
        let hasLoginForm = false;

        // goes through all forms checking if there is a form with a single password field.
        this.$("form").each((index, element) => {
            if (!hasLoginForm && this.$("input[type=password]", element).length === 1) {
                hasLoginForm = true;
            }
        });
        return hasLoginForm;
    }
}
