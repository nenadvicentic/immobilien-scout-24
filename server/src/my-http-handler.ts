import * as Boom from "boom";
import { IResult } from "dto";

import Parser from "./html-parser";
import HttpClient from "./http-client";

function onSuccess(url: string, html: string, reply: (data: any) => void): void {
    "use strict";

    const parser = new Parser(url, html);
    const links = parser.parseLinks();
    const result: IResult = {
        hasLoginForm: parser.hasLoginForm(),
        htmlVersion: parser.getHtmlVersion(),
        pageTitle: parser.getPageTitle(),
        headings: parser.getHeadingsCount(),
        internalLinksCount: links.internalLinksCount,
        externalLinksCount: links.externalLinksCount,
        inaccessibleLinksCount: links.inaccessibleLinksCount,
    };
    reply(result);
}

function onError(error: any, reply: (data: any) => void): void {
    "use strict";
    const httpError = Boom.badData("URL could not be processed");
    reply(httpError);
}

export default (request: any, reply: any): void => {
    const url = request.payload.url;
    const client = new HttpClient();
    client.fetch(
        url,
        (html) => onSuccess(url, html, reply),
        (error) => onError(error, reply)
    );
};
