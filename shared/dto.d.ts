declare module "dto" {
    export interface IResult {
        htmlVersion?: string;
        pageTitle?: string;
        headings: { name: string, count: number }[];
        internalLinksCount: number;
        externalLinksCount: number;
        inaccessibleLinksCount: number;
        hasLoginForm?: boolean;
    }
}