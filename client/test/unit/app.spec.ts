import {App} from "../../src/app";
import {HttpClient} from "aurelia-fetch-client";

describe("the app", () => {
  it("has message set", () => {
    expect(new App(new HttpClient()).message).toBe("Immobilien Scout 24 - Web-Application for Analyzing Web-Sites");
  });
});
