import * as Hapi from "hapi";
import * as Path from "path";

import myHttpHandler from "./my-http-handler";
const Inert = require("inert");

export default class Server {

    public initialize(): void {
        const server = new Hapi.Server({
            connections: {
                routes: {
                    files: {
                        relativeTo: Path.join(__dirname, "../../client/wwwroot"),
                    },
                },
            },
        });
        server.connection({
            host: "localhost",
            port: 8000,
        });

        server.register(Inert);

        server.route({
            handler: {
                directory: {
                    index: true,
                    path: ".",
                    redirectToSlash: true,
                },
            },
            method: "GET",
            path: "/{param*}",
        });

        server.route({
            handler: myHttpHandler,
            method: "POST",
            path: "/api/parse",
        });

        server.start((err) => {
            if (err) {
                throw err;
            }
            console.log("Server running at:", server.info.uri);
        });
    }
}
