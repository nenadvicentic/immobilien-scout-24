## Web-Application for Analyzing Web-Sites ##

### Requirements to run application ###

Make sure you have NodeJS and NPM installed. I used NodeJS v4.5.0 and NPM v3.10.5 bundled with NodeJS.

### Installation instructions ###
1. Create new project directory and copy `immobilien-scout-24-1.0.1.tgz` into the folder.

1. Optionally run `npm init` command within the root project directory and follow the wizard. This will create `project.json` file of in the folder.
 
1. In the same folder run command `npm install ./immobilien-scout-24-1.0.1.tgz` to unpack node module into `node_modules` folder.
 
1. Create `server.js` file in the root project directory and add following line there:
     
	     require('immobilien-scout-24');

#### Start web application
    
You will start application by running `node server.js` command, or, optionally, if you created `project.json` file , run `npm start` command. This will start web server on the port 8000.

Navigate your browser to `http://localhost:8000`. Web application should load there.

## How to (re)build application

### Requirements to build application ###

In addition to NodeJS/NPM dependencies which are required to run application, in order to build it from source-code, few more dependencies are needed.

Make sure you have `typings` installed, if not, run:

    npm i -g typings

For building client side application, make sure you have `aurelia-cli` installed:

    npm i -g aurelia-cli

*Note ***: Application was build and developed on Windows 10 machine and Visual Studio Code editor. It was not tested for forward/backward slash issues (mainly in aurelia.json config file).*

### Build server side

Open command line in the `node_modules/immobilien-scout-24/` folder inside of your project directory.

Run following commands:

    > npm install
    > typings install
    > npm run server:build

This will rebuild files in the `dist` folder.

### Build client side

Open command line in the `node_modules/immobilien-scout-24/client`

If you don't have `aurelia-cli` already installed, run command:

`npm i -g aurelia-cli`

Run following commands:

    > npm install
    > typings install
    > au build

This will rebuild bundled files in the `node_modules/immobilien-scout-24/client/wwwroot/` root folder.

## About the application ##

Application can be found in the `node_modules/immobilien-scout-24/`. Following is the structure of the project

    dist/ - Server-side transpiled code
    client/ - Client-side single page application source
	client/wwwroot - Complete client-side application minified and bundled
    server/ - Server-side source code

### Server side ###

Server-side application is build using Typescript 2.0.2 and transpiled to ECMAScript 2015 and CommonJS module system. It uses recommended node modules `hapi` and `cheerio` for the main functionality.

### Client side ###

Client side application is build using [Aurelia JavaScript client framework](http://aurelia.io/ "Aurelia"), TypeScript 2.0.2 and LESS based on Bootstrap 3.3.7. It's build around `aurelia-cli` project type with `requirejs` as a module loader. `requirejs` is used mainly because of it's loading performance and small devtools footprint. Typescript is transpiled to ECMAScript 5 and AMD module system.

Those are libraries/tools I would choose today for building production single page application today.

### Implementation decisions ###

##### Inaccessible links #####

It the code`inacessible link` is defined as `a` tag without valid `href` attribute. For example `a` tags used as hooks for JavaScript events.

##### Login page detection #####

`Login page` is defined as a form containing **strictly one** `<input type="password' />` element. This was done to avoid detecting pages for registration having additional "confirm password" fields as login pages. Issues might occur if login page contains additional hidden password fields.


#### Infrastructure for unit testing ####

Due to time constraints app specific unit tests were not implemented. However, infrastructure using Karma and Jasmine is present in the client project. This can be verified by running `au test` command from `node_modules/immobilien-scout-24/client/` folder. There will be 1 successful test passed.