import { HttpClient } from "aurelia-fetch-client";
import { DOM, inject } from "aurelia-framework";

@inject(HttpClient, DOM)
export class App {
  public message = "Immobilien Scout 24 - Web-Application for Analyzing Web-Sites";

  public urlToParse = "http://toastytech.com/evil/"; // default

  public result: any;
  public error: any;

  private keyPressHandler: (e: KeyboardEvent) => void;

  constructor(private http: HttpClient) {
    http.configure(config => {
        config
            .useStandardConfiguration()
            .withBaseUrl("api/");
    });

    this.keyPressHandler = (e) => {
      if (e.which === 13) {
        this.parseUrl();
      }
    };
  }

  public activate() {
    DOM.addEventListener("keypress", this.keyPressHandler, false);
  }
  public deactivate() {
    DOM.removeEventListener("keypress", this.keyPressHandler, false);
  }

  public parseUrl(): Promise<void> {
    this.result = this.error = null;
    return this.http.fetch("parse", {
            body: JSON.stringify({ url : this.urlToParse }),
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
          })
          .then(response => response.json())
          .catch(response => {
            response.json().then(data => {
              this.error = data;
            });
          })
          .then(data => {
            this.result = data;
          });
  }
}
