"use strict";
const cheerio = require("cheerio");
const url = require("url");
class HtmlParser {
    constructor(pageUrl, html) {
        this.html = html;
        this.$ = cheerio.load(html);
        this.pageUrl = url.parse(pageUrl);
    }
    /** Gets known HTML document type by trying to parse <!doctype> */
    getHtmlVersion() {
        const regex = /<!doctype html( PUBLIC \"-\/\/W3C\/\/DTD (.+?)\/\/\w\w\".*?)?>/gmi;
        const matches = regex.exec(this.html);
        if (matches == null || matches.length === 0) {
            return "Not specified";
        }
        // if no DTD description, default to HTML 5 <!doctype html>
        return matches[2] || "HTML 5";
    }
    /** Reads page title */
    getPageTitle() {
        return this.$("head>title").text();
    }
    getHeadingsCount() {
        const headings = new Array();
        ["h1", "h2", "h3", "h4", "h5", "h6"].forEach((val) => {
            headings.push({ name: val, count: this.$(val).length });
        });
        return headings;
    }
    /** Counts all internal, external on inacessible links on the page */
    parseLinks() {
        const result = {
            externalLinksCount: 0,
            inaccessibleLinksCount: 0,
            internalLinksCount: 0,
        };
        this.$("a").each((index, element) => {
            const href = this.$(element).attr("href");
            try {
                const linkUrl = url.parse(href);
                if (linkUrl.hostname == null ||
                    linkUrl.hostname === this.pageUrl.hostname) {
                    result.internalLinksCount++;
                }
                else {
                    result.externalLinksCount++;
                }
            }
            catch (ex) {
                // no or mallformed href, inaccessible link
                result.inaccessibleLinksCount++;
            }
        });
        return result;
    }
    /**
     * Searches for login form, based on assumption that login form has exactly one password field.
     * Registration pages usually have 2 password fields.
     */
    hasLoginForm() {
        let hasLoginForm = false;
        // goes through all forms checking if there is a form with a single password field.
        this.$("form").each((index, element) => {
            if (!hasLoginForm && this.$("input[type=password]", element).length === 1) {
                hasLoginForm = true;
            }
        });
        return hasLoginForm;
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HtmlParser;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHRtbC1wYXJzZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zZXJ2ZXIvc3JjL2h0bWwtcGFyc2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxNQUFZLE9BQU8sV0FBTSxTQUFTLENBQUMsQ0FBQTtBQUNuQyxNQUFZLEdBQUcsV0FBTSxLQUFLLENBQUMsQ0FBQTtBQUUzQjtJQUlJLFlBQVksT0FBZSxFQUFTLElBQVk7UUFBWixTQUFJLEdBQUosSUFBSSxDQUFRO1FBQzVDLElBQUksQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELGtFQUFrRTtJQUMzRCxjQUFjO1FBQ2pCLE1BQU0sS0FBSyxHQUFHLG1FQUFtRSxDQUFDO1FBRWxGLE1BQU0sT0FBTyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3RDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sSUFBSSxJQUFJLElBQUksT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFDLE1BQU0sQ0FBQyxlQUFlLENBQUM7UUFDM0IsQ0FBQztRQUNELDJEQUEyRDtRQUMzRCxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQztJQUNsQyxDQUFDO0lBRUQsdUJBQXVCO0lBQ2hCLFlBQVk7UUFDZixNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRU0sZ0JBQWdCO1FBQ25CLE1BQU0sUUFBUSxHQUFHLElBQUksS0FBSyxFQUFtQyxDQUFDO1FBRTlELENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHO1lBQzdDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDNUQsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxxRUFBcUU7SUFDOUQsVUFBVTtRQUNiLE1BQU0sTUFBTSxHQUFHO1lBQ1gsa0JBQWtCLEVBQUUsQ0FBQztZQUNyQixzQkFBc0IsRUFBRSxDQUFDO1lBQ3pCLGtCQUFrQixFQUFFLENBQUM7U0FDeEIsQ0FBQztRQUNGLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLE9BQU87WUFDNUIsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDO2dCQUNELE1BQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2hDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLElBQUksSUFBSTtvQkFDeEIsT0FBTyxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQzdDLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUNoQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE1BQU0sQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2dCQUNoQyxDQUFDO1lBQ0wsQ0FBRTtZQUFBLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsMkNBQTJDO2dCQUMzQyxNQUFNLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztZQUNwQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7O09BR0c7SUFDSSxZQUFZO1FBQ2YsSUFBSSxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXpCLG1GQUFtRjtRQUNuRixJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxPQUFPO1lBQy9CLEVBQUUsQ0FBQyxDQUFDLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsc0JBQXNCLEVBQUUsT0FBTyxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDeEIsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLFlBQVksQ0FBQztJQUN4QixDQUFDO0FBQ0wsQ0FBQztBQTVFRDs0QkE0RUMsQ0FBQSJ9