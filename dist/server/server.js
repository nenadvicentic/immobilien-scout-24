"use strict";
const Hapi = require("hapi");
const Path = require("path");
const my_http_handler_1 = require("./my-http-handler");
const Inert = require("inert");
class Server {
    initialize() {
        const server = new Hapi.Server({
            connections: {
                routes: {
                    files: {
                        relativeTo: Path.join(__dirname, "../../client/wwwroot"),
                    },
                },
            },
        });
        server.connection({
            host: "localhost",
            port: 8000,
        });
        server.register(Inert);
        server.route({
            handler: {
                directory: {
                    index: true,
                    path: ".",
                    redirectToSlash: true,
                },
            },
            method: "GET",
            path: "/{param*}",
        });
        server.route({
            handler: my_http_handler_1.default,
            method: "POST",
            path: "/api/parse",
        });
        server.start((err) => {
            if (err) {
                throw err;
            }
            console.log("Server running at:", server.info.uri);
        });
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Server;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VydmVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vc2VydmVyL3NyYy9zZXJ2ZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE1BQVksSUFBSSxXQUFNLE1BQU0sQ0FBQyxDQUFBO0FBQzdCLE1BQVksSUFBSSxXQUFNLE1BQU0sQ0FBQyxDQUFBO0FBRTdCLGtDQUEwQixtQkFBbUIsQ0FBQyxDQUFBO0FBQzlDLE1BQU0sS0FBSyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUUvQjtJQUVXLFVBQVU7UUFDYixNQUFNLE1BQU0sR0FBRyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDM0IsV0FBVyxFQUFFO2dCQUNULE1BQU0sRUFBRTtvQkFDSixLQUFLLEVBQUU7d0JBQ0gsVUFBVSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLHNCQUFzQixDQUFDO3FCQUMzRDtpQkFDSjthQUNKO1NBQ0osQ0FBQyxDQUFDO1FBQ0gsTUFBTSxDQUFDLFVBQVUsQ0FBQztZQUNkLElBQUksRUFBRSxXQUFXO1lBQ2pCLElBQUksRUFBRSxJQUFJO1NBQ2IsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUV2QixNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ1QsT0FBTyxFQUFFO2dCQUNMLFNBQVMsRUFBRTtvQkFDUCxLQUFLLEVBQUUsSUFBSTtvQkFDWCxJQUFJLEVBQUUsR0FBRztvQkFDVCxlQUFlLEVBQUUsSUFBSTtpQkFDeEI7YUFDSjtZQUNELE1BQU0sRUFBRSxLQUFLO1lBQ2IsSUFBSSxFQUFFLFdBQVc7U0FDcEIsQ0FBQyxDQUFDO1FBRUgsTUFBTSxDQUFDLEtBQUssQ0FBQztZQUNULE9BQU8sRUFBRSx5QkFBYTtZQUN0QixNQUFNLEVBQUUsTUFBTTtZQUNkLElBQUksRUFBRSxZQUFZO1NBQ3JCLENBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHO1lBQ2IsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDTixNQUFNLEdBQUcsQ0FBQztZQUNkLENBQUM7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdkQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0FBQ0wsQ0FBQztBQTVDRDt3QkE0Q0MsQ0FBQSJ9